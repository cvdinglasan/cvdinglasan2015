class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})

    def createPost(self, post, feeling, username):
        self.conn.insert({'post':post, 'feeling':feeling, 'username': username})