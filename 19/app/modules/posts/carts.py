from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session


mod = Blueprint('cart', __name__)

@mod.route('/', methods=['GET', 'POST'])
def cart_list():
	items = g.cartdb.getCart()


@mod.route('/add', methods= ['POST'])
def add_to_cart():
	add_item = request.form['post']
	price = request.form['price']
	g.cartdb.createCart(add_item, price)
	flash('New item added to cart', 'add_to_cart_success')
