class CartsDB:
	def __init__(self, conn):
		self.conn = conn

	def getCart(self):
		return self.conn.find()

	def createCart(self, product, price):
		self.conn.insert({'product':product, 'price':price})