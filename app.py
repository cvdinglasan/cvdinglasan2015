from flask import Flask
from flask import render_template
from flask import request
app = Flask(__name__)

#add route
@app.route('/')
def hello_world():
	return 'Hello World!'

@app.route('/<username>', methods=['GET', 'POST'])
def hello_username(username):
	if request.method == 'POST':
		for x in request.form:
			print x + ' ' + str(request.form[x])
		return render_template('hello_post.html', name=username)
	elif request.method == 'GET':
		for x in request.args:
			print x + ' ' + str(request.args[x])
		return render_template('hello.html', name=username)
	return 'Not supported'
	#return 'Hello %s!' %username

@app.route('/<int:user_id>')
def hello_user_id(user_id):
	return 'Printing user id: %s' %user_id

@app.route('/about')
def about_page():
	return 'This is the about page!'


if __name__ == '__main__':
	app.run()
