from bson.objectid import ObjectId

class CartsDB:
	def __init__(self, conn):
		self.conn = conn

	def getCart(self):
		return self.conn.find()

	def createCart(self, post, price):
		self.conn.insert({'post':post, 'price':price})