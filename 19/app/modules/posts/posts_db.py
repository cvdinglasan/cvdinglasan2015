from bson.objectid import ObjectId

class ItemDB:
    def __init__(self, conn):
        self.conn = conn

    def getItems(self):
        return self.conn.find()

    def createItem(self, post, price):
        self.conn.insert({'post':post, 'price':price})

