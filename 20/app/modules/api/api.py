from flask import Blueprint
from flask import jsonify
from flask import request

mod = Blueprint('api', __name__)

sample_data = [
        {'author':'banana', 'post':'23'},
        {'author':'mango', 'post':'24'},
        {'author':'pineapple', 'post':'25'},
    ]

@mod.route('/getsampledata', methods=['GET','POST'])
def get_sample_data():
    print request.form.keys()
    if request.method=='POST':
        sample_data.append({
            'author':request.form['author'], 
            'post':request.form['post'], 
        })
    return jsonify(posts=sample_data)