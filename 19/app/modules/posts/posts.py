from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('post', __name__)


@mod.route('/', methods = ['GET', 'POST'])
def item_list():
    items = g.itemdb.getItems()
    print items[0]
    carts = g.cartdb.getCart()
    if request.method == "POST":
        m = [x for x in request.form.getlist('quantity')]
        for x in xrange(0, len(m)):
            if int(m[x]) != 0:
                g.cartdb.createCart(items[x]['post'], int(items[x]['price']) * int(m[x]))

    return render_template('posts/post.html', items=items, carts=carts)


@mod.route('/add', methods=['POST'])
def add_item():
    add_item = request.form['post']
    price = request.form['price']
    g.itemdb.createItem(add_item, price)
    flash('New item added to inventory!', 'add_item_success')
    return redirect(url_for('.item_list'))
'''
@mod.route('/cart', methods=['POST'])
def add_to_cart():
    add_item = request.form['add_item']
    price = request.form['price']
    quantity = request.form['quantity']
    g.postsdb.addCart(add_item, price, quantity, session['username'])
    flash('New item added to cart!', 'add_to_cart_success')
    return redirect(url_for('.add_cart'))

'''

