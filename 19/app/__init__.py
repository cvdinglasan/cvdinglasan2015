from flask import Flask
from flask import g

from modules import default
from modules import posts
from modules import util
from modules import carts 

from pymongo import MongoClient

app = Flask(__name__)

app.session_interface = util.MongoSessionInterface(db='sessions')

def get_main_db():
    client = MongoClient('mongodb://localhost:27017/')
    maindb = client.itemdb
    return maindb

@app.before_request
def before_request():
    mainDb = get_main_db()
    g.usersdb = posts.UserDB(conn=mainDb.users)
    g.itemdb = posts.ItemDB(conn=mainDb.posts)
    g.cartdb = carts.CartsDB(conn=mainDb.carts)
   

app.register_blueprint(default.mod)
app.register_blueprint(posts.mod, url_prefix="/posts")
#app.register_blueprint(carts.mod)